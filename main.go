package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	post_service "gitlab.com/test/fetch_service/protos/post_service"
	"google.golang.org/grpc"
)

type Post struct {
	Id     uint64 `json:"id"`
	UserID uint64 `json:"user_id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

type Response struct {
	Meta interface{} `json:"meta"`
	Data []Post      `json:"data"`
}

var logger *log.Logger
var postServiceClient post_service.PostServiceClient

func main() {

	logger = log.Default()
	logger.SetPrefix("fetch_service")

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "localhost", 9000),
		grpc.WithInsecure())
	if err != nil {
		panic(fmt.Errorf("recipe service dial host: %s port: %d err: %s",
			"localhost", 9000, err))
	}

	postServiceClient = post_service.NewPostServiceClient(connPost)

	fetchData(1)
}

func fetchData(page int) error {

	url := fmt.Sprintf("https://gorest.co.in/public/v1/posts?page=%d", page)
	method := "GET"

	payload := strings.NewReader("")

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return err
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}

	var response Response
	json.Unmarshal(body, &response)

	posts := []*post_service.Post{}
	
	respData, _ := json.Marshal(response.Data)
	json.Unmarshal(respData, &posts)

	request := post_service.CreateMultiplePostRequest{Posts: posts}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = postServiceClient.CreateMultiple(ctx, &request)
	if err != nil {
		logger.Println("Error creating post err:", err)
	}

	return nil
}
