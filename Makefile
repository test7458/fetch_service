
.PHONY: vendor

run:
	go run ./cmd/main.go

tidy: 
	go mod tidy

vendor:
	go mod vendor

proto-gen:
	./gen-proto.sh
