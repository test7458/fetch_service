package grpcclient

import (
	"fmt"
	"sync"

	"gitlab.com/test/fetch_service/config"
	post_service "gitlab.com/test/fetch_service/protos/post_service"
	"google.golang.org/grpc"
)

var cfg = config.Get()
var (
	oncePostService     sync.Once
	instancePostService post_service.PostServiceClient
)

// PostService ...
func PostService() post_service.PostServiceClient {
	oncePostService.Do(func() {
		connPost, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("post service dial host: %s port:%d err: %s",
				cfg.PostServiceHost, cfg.PostServicePort, err))
		}

		instancePostService = post_service.NewPostServiceClient(connPost)
	})

	return instancePostService
}
