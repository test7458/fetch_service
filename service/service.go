package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	grpc_client "gitlab.com/test/fetch_service/grpc_client"
	pb "gitlab.com/test/fetch_service/protos/fetch_service"
	post_pb "gitlab.com/test/fetch_service/protos/post_service"
)

type fetchService struct {
	logger *log.Logger
}

func NewFetchServiceServer(l *log.Logger) pb.FetchServiceServer {

	return &fetchService{logger: l}
}

type Post struct {
	Id     uint64 `json:"id"`
	UserID uint64 `json:"user_id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

type Response struct {
	Meta interface{} `json:"meta"`
	Data []Post      `json:"data"`
}

const (
	urlPrefix = "https://gorest.co.in/public/v1/posts?page=%d"
)

func (s *fetchService) Fetch(c context.Context, r *pb.FetchPostRequest) (*pb.FetchPostResponse, error) {

	url := fmt.Sprintf(urlPrefix, r.GetPage())
	method := "GET"

	payload := strings.NewReader("")

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var response Response
	json.Unmarshal(body, &response)

	posts := []*post_pb.Post{}

	respData, _ := json.Marshal(response.Data)
	json.Unmarshal(respData, &posts)

	request := post_pb.CreateMultiplePostRequest{Posts: posts}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err = grpc_client.PostService().CreateMultiple(ctx, &request)
	if err != nil {
		s.logger.Println("Error creating post err:", err)
	}

	return &pb.FetchPostResponse{}, nil
}
