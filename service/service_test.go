package service_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"

	pb "gitlab.com/test/fetch_service/protos/fetch_service"
	"gitlab.com/test/fetch_service/service"
)

var fetchServiceTest pb.FetchServiceServer

func init() {
	const path = ".env"
	if info, err := os.Stat(path); !os.IsNotExist(err) {
		if !info.IsDir() {
			godotenv.Load(path)
			if err != nil {
				fmt.Println("Err:", err)
			}
		}
	} else {
		fmt.Println("Not exists")
	}
	log := log.Default()

	log.SetPrefix("fetch_service_test: ")
	fetchServiceTest = service.NewFetchServiceServer(log)
}
func TestFetchPost(t *testing.T) {

	testCases := make([]*pb.FetchPostRequest, 50)

	for i := 0; i < 50; i++ {
		testCases[i] = &pb.FetchPostRequest{
			Page: uint32(i+1),
		}
	}

	for _, testCase := range testCases {
		t.Run("Fetch posst", func(t *testing.T) {

			_, err := fetchServiceTest.Fetch(context.Background(), testCase)
			assert.Equal(t, nil, err, "Error in fetching posts")

		})
	}

}
