package main

import (
	"net"

	"log"

	pb "gitlab.com/test/fetch_service/protos/fetch_service"

	"gitlab.com/test/fetch_service/service"

	"go.uber.org/zap"

	"google.golang.org/grpc"

	"gitlab.com/test/fetch_service/config"
)

func main() {

	cfg := config.Get()
	log := log.Default()

	log.SetPrefix("fetch_service: ")
	// =========================================================================
	// =========================================================================
	listen, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatalf("error listening tcp port %v", zap.Error(err))
	}

	server := grpc.NewServer()
	postServiceServer := service.NewFetchServiceServer(log)
	pb.RegisterFetchServiceServer(server, postServiceServer)

	log.Println("main: server running on port", cfg.RPCPort)

	if err := server.Serve(listen); err != nil {
		log.Fatalf("error listening: %v", zap.Error(err))
	}

}
